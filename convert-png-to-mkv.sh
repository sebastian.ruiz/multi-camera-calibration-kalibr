#!/bin/sh
echo This script will loop over unity-cams/cam0, ..., unity-cams/cam4
echo and create mkv files and put them in the calibration directory.

for i in 0 1 2 3 4
do
	echo "Generating mkv for cam$i"
	ffmpeg -framerate 1 -i unity-cams/cam$i/%d.png -c:v libx264 -crf 12 calibration/cam$i.mkv
done
