import numpy as np
from PIL import Image
import os
import time
from tqdm import tqdm
import multiprocessing
import cv2
import regex as re

# ==========================================================================
# EDIT THESE VARIABLES
# ==========================================================================

flirctrl_name = "2020.10.27-15.47"
flirctrl_folder = flirctrl_name  # might have to set to "data/" + flirctrl_name
formatted_folder = flirctrl_folder + "_formatted"
num_images_per_camera = 300
# cameras = ["camA", "camB", "camH", "camK", "camN"]
cameras = ["cam0", "cam1", "cam2", "cam3", "cam4"]
use_nanoseconds = False  # set to True for Kalibr calibration
rescale_to_1024_by_768 = True
multi_threaded = True

# convert from .avi files to kalibr format
convert_from_avi = True

# Misc: Convert to mkv files
convert_to_mkv = False  # set to False for Kalibr calibration
framerate = 1



# ==========================================================================
# DO NOT EDIT BELOW THIS LINE
# ==========================================================================

def worker(cam_index, camera):
    """thread worker function"""
    print("converting camera " + str(cam_index) + ".")

    # flirctrl indexes images starting from 1
    if convert_to_mkv:
        print("converting to .mkv")

        image_folder = os.path.join(working_dir, flirctrl_folder)
        video_name = os.path.join(working_dir, formatted_folder, "cam" + str(cam_index) + ".mkv")

        print(image_folder)

        images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
        images.sort(key=lambda f: int(re.sub('\D', '', f)))
        print("images", images)
        frame = cv2.imread(os.path.join(image_folder, images[0]))
        height, width, layers = frame.shape

        video = cv2.VideoWriter(video_name, 0, 1, (width, height))
        # video = cv2.VideoWriter(video_name, cv2.VideoWriter_fourcc(*'XVID'), 30, (width, height))

        for image in images:
            video.write(cv2.imread(os.path.join(image_folder, image)))

        video.release()
    else:

        if convert_from_avi:
            print("converting from .avi files to kalibr format")

            print("video:", cam_index, camera)
            video_folder = os.path.join(working_dir, flirctrl_folder)
            video_path = os.path.join(video_folder, camera + str(".avi"))

            # create the camera directory
            new_camera_folder = os.path.join(working_dir, formatted_folder, "cam" + str(cam_index))
            os.mkdir(new_camera_folder)

            # Read the video from specified path
            cam = cv2.VideoCapture(video_path)

            currentframe = 0

            while True:
                # reading from frame
                ret, frame = cam.read()

                if ret:
                    # if video is still left continue creating images
                    if use_nanoseconds:
                        new_img_filename = str(time_now + currentframe * 10 ** 9) + ".png"
                    else:
                        new_img_filename = str(currentframe) + ".png"

                    # writing the extracted images
                    cv2.imwrite(os.path.join(new_camera_folder, new_img_filename), frame)

                    # increasing counter so that it will
                    # show how many frames are created
                    print(camera, currentframe)
                    currentframe += 1
                else:
                    break

            # Release all space and windows once done
            cam.release()

        else:
            print("converting from .bmp files to kalibr format")

            # create the camera directory
            os.mkdir(os.path.join(working_dir, formatted_folder, "cam" + str(cam_index)))
            for img_index in tqdm(np.arange(1, num_images_per_camera + 1)):
                img_filename = flirctrl_name + "-" + camera + "-" + str(img_index) + ".BMP"
                if use_nanoseconds:
                    new_img_filename = str(time_now + (img_index - 1) * 10**9) + ".png"
                else:
                    new_img_filename = str(img_index - 1) + ".png"
                if os.path.isfile(os.path.join(working_dir, flirctrl_folder, img_filename)):
                    # copy, convert to .png and rename image
                    im = Image.open(os.path.join(working_dir, flirctrl_folder, img_filename))
                    if rescale_to_1024_by_768:
                        im.thumbnail((1024, 768))
                    im.save(os.path.join(working_dir, formatted_folder, "cam" + str(cam_index), new_img_filename))

                else:
                    print("ERROR! " + os.path.join(working_dir, flirctrl_folder, img_filename) + " is not a file!")
                    assert False

    return


if __name__ == '__main__':
    time_now = int(round(time.time()) * 10**9)  # in nanoseconds
    print(time_now)
    working_dir = os.getcwd()  # current working directory
    # create formatted cameras directory
    os.mkdir(os.path.join(working_dir, formatted_folder))

    jobs = []
    for cam_index, camera in enumerate(cameras):
        if multi_threaded:
            p = multiprocessing.Process(target=worker, args=(cam_index, camera, ))
            jobs.append(p)
            p.start()
        else:
            worker(cam_index, camera)




