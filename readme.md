# Multi-Camera Calibration using [ROS Kalibr](https://github.com/ethz-asl/kalibr)

Preferred setup using [Docker ROS Kalibr](https://github.com/adujardin/docker-ros-kalibr).


## My setup:


1. Start the container using `docker-compose run --rm kalibr`.

2. Go to `cd data`.

3. Create new folder of images in the [Kalibr bag format](https://github.com/ethz-asl/kalibr/wiki/bag-format). Use the `convert_flirctrl_format.py` script to help.

4. Create the bag:

```
kalibr_bagcreater --folder calibration_images --output-bag awesome.bag
```


5. Carry out calibration. Use the `checkerboard.yaml` or `aprilboard.yaml` depending on the calibration board you are using.

For checkerboard:

```
kalibr_calibrate_cameras --target checkerboard.yaml --bag awesome.bag --models pinhole-equi pinhole-equi pinhole-equi pinhole-equi pinhole-equi --topics /cam0/image_raw /cam1/image_raw /cam2/image_raw /cam3/image_raw /cam4/image_raw
```

For aprilboard:
```
kalibr_calibrate_cameras --target aprilboard.yaml --bag awesome.bag --models pinhole-equi pinhole-equi pinhole-equi pinhole-equi pinhole-equi --topics /cam0/image_raw /cam1/image_raw /cam2/image_raw /cam3/image_raw /cam4/image_raw
```

Output of calibration is a `camchain.yaml` file. [See here](https://github.com/ethz-asl/kalibr/wiki/yaml-formats) for what the parameters mean.